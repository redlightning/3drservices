# 3DRServices

A clone, expansion, and update to [DroneKit for Android](https://github.com/dronekit/dronekit-android)

# DroneKit-Android

DroneKit-Android helps you create powerful Android apps for UAVs.

## Overview

[DroneKit-Android](http://android.dronekit.io/) is the implementation of [DroneKit](http://dronekit.io/) on Android.

The API provides interfaces that apps can use to control copters, planes, and ground vehicles. It is compatible with vehicles that communicate using the MAVLink protocol (including most vehicles made by 3DR and other members of the DroneCode foundation), and is validated against the open-source [ArduPilot flight control platform](http://ardupilot.com).

This project implements the [DroneKit-Android Client library](http://android.dronekit.io) which allows developers to leverage the DroneKit API.

## Getting Started

The [Getting Started](http://android.dronekit.io/getting_started.html) guide explains how to set up a DroneKit project in Android Studio, and start the autopilot simulator (SITL) for testing during development.

Once you've got DroneKit set up, the [First App: Hello Drone](http://android.dronekit.io/first_app.html) tutorial provides a step-by-step guide to creating a basic DroneKit-Android app and connecting it to a vehicle. 

### Examples

Source code and documentation for some of the apps built with DroneKit is linked below:
* [Tower](https://github.com/DroidPlanner/Tower)
* [Tower-Wear](https://github.com/DroidPlanner/tower-wear)
* [Tower-Pebble](https://github.com/DroidPlanner/dp-pebble) ([documentation here](http://android.dronekit.io/pebble_app.html))


## Resources

Project documentation is provided at http://android.dronekit.io/. This includes getting stated and tutorial material, and has links to [examples](#examples) and the full [Javadoc](http://android.dronekit.io/javadoc/) API Reference.

The [DroneKit Forums](http://discuss.dronekit.io) are the best place to ask for technical support on how to use the library. You can also check out our [Gitter channel](https://gitter.im/dronekit/dronekit-android) though we prefer posts on the forums where possible.

* **Documentation:** http://android.dronekit.io/
  * **API Reference:** http://android.dronekit.io/javadoc/
  * **Examples:** http://android.dronekit.io/resources.html#example-apps
* **Forums:** [http://discuss.dronekit.io/](http://discuss.dronekit.io)
* **Gitter:** https://gitter.im/dronekit/dronekit-android (we prefer posts on the forums!)

## Users and contributors wanted!

## Licence

3DRServices is made available under the permissive open source Apache 2.0 License
