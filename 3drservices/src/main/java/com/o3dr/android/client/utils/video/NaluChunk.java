package com.o3dr.android.client.utils.video;

import java.nio.ByteBuffer;

/**
 * Created by fhuya on 12/4/14.
 */
public class NaluChunk {
    protected static final byte[] START_CODE = {0, 0, 0, 1};
    public static final int SPS_NAL_TYPE = 7;
    public static final int PPS_NAL_TYPE = 8;

    public final ByteBuffer[] payloads;

    public NaluChunk(int payloadCount, int payloadSize, byte[] payloadInitData) {
        this.payloads = new ByteBuffer[payloadCount];
        for (int i = 0; i < payloadCount; i++) {
            this.payloads[i] = ByteBuffer.allocate(payloadSize);
            if (payloadInitData != null) {
                this.payloads[i].put(payloadInitData);
                this.payloads[i].mark();
            }
        }
    }

    private int type;
    private int sequenceNumber;
    private int flags;
    private long presentationTime;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public int getFlags() {
        return flags;
    }

    public void setFlags(int flags) {
        this.flags = flags;
    }

    public long getPresentationTime() {
        return presentationTime;
    }

    public void setPresentationTime(long presentationTime) {
        this.presentationTime = presentationTime;
    }
}
