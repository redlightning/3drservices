package com.o3dr.android.client.utils;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;

import net.redlightning.a3drservices.R;

import androidx.fragment.app.FragmentActivity;


/**
 * Created by fhuya on 12/15/14.
 */
public class UpdateServiceDialog extends FragmentActivity {
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_service_dialog);

        final Button cancelButton = findViewById(R.id.dialog_cancel_button);
        cancelButton.setOnClickListener(v -> finish());

        final Button installButton = findViewById(R.id.dialog_update_button);
        installButton.setOnClickListener(v -> {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=org.droidplanner.services.android")));
            finish();
        });
    }
}
