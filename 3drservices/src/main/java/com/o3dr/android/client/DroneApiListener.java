package com.o3dr.android.client;

import android.content.Context;

import com.o3dr.services.android.lib.drone.connection.ConnectionResult;
import com.o3dr.services.android.lib.model.IApiListener;
import com.o3dr.services.android.lib.util.version.VersionUtils;

import net.redlightning.a3drservices.BuildConfig;

import timber.log.Timber;

/**
 * Created by fhuya on 12/15/14.
 */
public class DroneApiListener extends IApiListener.Stub {

    private final Context context;

    public DroneApiListener(Context context){
        this.context = context;
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Timber.w("DroneApi connection failed %s", connectionResult.toString());
    }

    @Override
    public int getClientVersionCode() {
        return BuildConfig.VERSION_CODE;
    }

    @Override
    public int getApiVersionCode(){
        return VersionUtils.getCoreLibVersion(this.context);
    }
}
