package com.o3dr.android.client.apis.solo;

import com.o3dr.android.client.Drone;
import com.o3dr.services.android.lib.drone.companion.solo.tlv.TLVPacket;
import com.o3dr.services.android.lib.model.AbstractCommandListener;

import java.util.concurrent.ConcurrentHashMap;

import androidx.annotation.NonNull;

/**
 * Created by Fredia Huya-Kouadio on 7/31/15.
 */
public class SoloMessageApi extends SoloApi {

    private static final ConcurrentHashMap<Drone, SoloMessageApi> apiCache = new ConcurrentHashMap<>();
    private static final Builder<SoloMessageApi> apiBuilder = SoloMessageApi::new;

    @NonNull
    public static SoloMessageApi getApi(@NonNull final Drone drone){
        return getApi(drone, apiCache, apiBuilder);
    }

    protected SoloMessageApi(@NonNull Drone drone) {
        super(drone);
    }

    @Override
    public void sendMessage(TLVPacket messagePacket, AbstractCommandListener listener){
        super.sendMessage(messagePacket, listener);
    }
}
