package com.o3dr.android.client.apis;

import android.util.Log;

import com.o3dr.android.client.Drone;
import com.o3dr.services.android.lib.model.AbstractCommandListener;

import java.util.concurrent.ConcurrentHashMap;

import androidx.annotation.NonNull;
import timber.log.Timber;

/**
 * Common interface for the drone set of api classes.
 * Created by Fredia Huya-Kouadio on 7/5/15.
 */
public abstract class Api {
    private static final String TAG = Api.class.getCanonicalName();

    protected interface Builder<T extends Api> {
        @NonNull
        T build(@NonNull Drone drone);
    }

    /**
     * Retrieves the api instance bound to the given Drone object.
     * @param drone Drone object
     * @param apiCache Used to retrieve the api instance if it exists, or store it if it doesn't exist.
     * @param apiBuilder Api instance generator.
     * @param <T> Specific api instance type.
     * @return The matching Api instance.
     */
    protected @NonNull static <T extends Api> T getApi(@NonNull Drone drone, @NonNull ConcurrentHashMap<Drone, T> apiCache, @NonNull Api.Builder<T> apiBuilder){
        if(drone == null || apiCache == null)
            return null;

        T apiInstance = apiCache.get(drone);
        if(apiInstance == null && apiBuilder != null){
            @NonNull
            T newInstance = apiBuilder.build(drone);
            T putInstance = apiCache.putIfAbsent(drone, newInstance);
            if(putInstance != null) {
                return putInstance;
            } else {
                Log.w(TAG, "Adding API instance to API Cache failed, previousInstance is null");
                return newInstance;
            }
        }

        return apiInstance;
    }

    protected static void postSuccessEvent(final AbstractCommandListener listener){
        if(listener != null){
            listener.onSuccess();
        }
    }

    protected static void postErrorEvent(int error, AbstractCommandListener listener){
        if(listener != null){
            listener.onError(error);
        }
    }

    protected static void postTimeoutEvent(AbstractCommandListener listener){
        if(listener != null){
            listener.onTimeout();
        }
    }
}
