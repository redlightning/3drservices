package com.o3dr.services.android.lib.model;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

import com.o3dr.services.android.lib.model.action.Action;

import androidx.annotation.NonNull;

public interface IDroneApi extends IInterface {

    void addAttributesObserver(IObserver observer) throws RemoteException;

    void addMavlinkObserver(IMavlinkObserver mavlinkObserver) throws RemoteException;

    void executeAction(Action action, ICommandListener commandListener) throws RemoteException;

    void executeAsyncAction(Action action, ICommandListener commandListener) throws RemoteException;

    Bundle getAttribute(String attributeName) throws RemoteException;

    void performAction(Action action) throws RemoteException;

    void performAsyncAction(Action action) throws RemoteException;

    void removeAttributesObserver(IObserver observer) throws RemoteException;

    void removeMavlinkObserver(IMavlinkObserver mavlinkObserver) throws RemoteException;

    abstract class Stub extends Binder implements IDroneApi {
        private static final String DESCRIPTOR = "com.o3dr.services.android.lib.model.IDroneApi";
        private static final int TRANSACTION_ADD_ATTRIBUTES_OBSERVER = 4;
        private static final int TRANSACTION_ADD_MAVLINK_OBSERVER = 6;
        private static final int TRANSACTION_EXECUTE_ACTION = 8;
        private static final int TRANSACTION_EXECUTE_ASYNC_ACTION = 9;
        private static final int TRANSACTION_GET_ATTRIBUTE = 1;
        private static final int TRANSACTION_PERFORM_ACTION = 2;
        private static final int TRANSACTION_PERFORM_ASYNC_ACTION = 3;
        private static final int TRANSACTION_REMOVE_ATTRIBUTES_OBSERVER = 5;
        private static final int TRANSACTION_REMOVE_MAVLINK_OBSERVER = 7;

        public Stub() {
            this.attachInterface(this, DESCRIPTOR);
        }

        @Override
        public IBinder asBinder() {
            return this;
        }

        @Override
        public boolean onTransact(int code, @NonNull Parcel data, Parcel reply, int flags) throws RemoteException {
            boolean result = true;
            Action action = null;
            switch (code) {
                case TRANSACTION_GET_ATTRIBUTE:
                    data.enforceInterface(DESCRIPTOR);
                    Bundle bundle = this.getAttribute(data.readString());
                    reply.writeNoException();
                    if (bundle != null) {
                        reply.writeInt(1);
                        bundle.writeToParcel(reply, Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
                    } else {
                        reply.writeInt(0);
                    }
                    break;
                case TRANSACTION_PERFORM_ACTION:
                    data.enforceInterface(DESCRIPTOR);
                    if (data.readInt() != 0) {
                        action = Action.CREATOR.createFromParcel(data);
                    }

                    this.performAction(action);
                    reply.writeNoException();
                    if (action != null) {
                        reply.writeInt(1);
                        action.writeToParcel(reply, Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
                    } else {
                        reply.writeInt(0);
                    }
                    break;
                case TRANSACTION_PERFORM_ASYNC_ACTION:
                    data.enforceInterface(DESCRIPTOR);
                    if (data.readInt() != 0) {
                        action = Action.CREATOR.createFromParcel(data);
                    }

                    this.performAsyncAction(action);
                    break;
                case TRANSACTION_ADD_ATTRIBUTES_OBSERVER:
                    data.enforceInterface(DESCRIPTOR);
                    this.addAttributesObserver(IObserver.Stub.asInterface(data.readStrongBinder()));
                    break;
                case TRANSACTION_REMOVE_ATTRIBUTES_OBSERVER:
                    data.enforceInterface(DESCRIPTOR);
                    this.removeAttributesObserver(IObserver.Stub.asInterface(data.readStrongBinder()));
                    break;
                case TRANSACTION_ADD_MAVLINK_OBSERVER:
                    data.enforceInterface(DESCRIPTOR);
                    this.addMavlinkObserver(IMavlinkObserver.Stub.asInterface(data.readStrongBinder()));
                    break;
                case TRANSACTION_REMOVE_MAVLINK_OBSERVER:
                    data.enforceInterface(DESCRIPTOR);
                    this.removeMavlinkObserver(IMavlinkObserver.Stub.asInterface(data.readStrongBinder()));
                    break;
                case TRANSACTION_EXECUTE_ACTION:
                    data.enforceInterface(DESCRIPTOR);
                    if (data.readInt() != 0) {
                        action = Action.CREATOR.createFromParcel(data);
                    }

                    this.executeAction(action, ICommandListener.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    if (action != null) {
                        reply.writeInt(1);
                        action.writeToParcel(reply, Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
                    } else {
                        reply.writeInt(0);
                    }
                    break;
                case 9:
                    data.enforceInterface(DESCRIPTOR);
                    if (data.readInt() != 0) {
                        action = Action.CREATOR.createFromParcel(data);
                    }

                    this.executeAsyncAction(action, ICommandListener.Stub.asInterface(data.readStrongBinder())
                    );
                    break;
                case 1598968902:
                    reply.writeString(DESCRIPTOR);
                    result = super.onTransact(code, data, reply, flags);
                    break;
                default:
                    result = super.onTransact(code, data, reply, flags);
            }

            return result;
        }

        public static IDroneApi asInterface(IBinder binder) {
            IDroneApi droneApi;
            if (binder == null) {
                droneApi = null;
            } else {
                IInterface localInterface = binder.queryLocalInterface(DESCRIPTOR);
                if (localInterface instanceof IDroneApi) {
                    droneApi = (IDroneApi) localInterface;
                } else {
                    droneApi = new Proxy(binder);
                }
            }

            return droneApi;
        }

        private static class Proxy implements IDroneApi {
            IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            @Override
            public void addAttributesObserver(IObserver observer) throws RemoteException {
                IBinder binder = null;
                Parcel data = Parcel.obtain();

                data.writeInterfaceToken(DESCRIPTOR);

                if (observer != null) {
                    binder = observer.asBinder();
                }

                try {
                    data.writeStrongBinder(binder);
                    this.mRemote.transact(TRANSACTION_ADD_ATTRIBUTES_OBSERVER, data, null, 1);
                } catch (RemoteException e) {
                    data.recycle();
                    throw e;
                }

                data.recycle();
            }

            @Override
            public void addMavlinkObserver(IMavlinkObserver mavlinkObserver) throws RemoteException {
                IBinder binder = null;
                Parcel data = Parcel.obtain();

                data.writeInterfaceToken(DESCRIPTOR);

                if (mavlinkObserver != null) {
                    binder = mavlinkObserver.asBinder();
                }

                try {
                    data.writeStrongBinder(binder);
                    this.mRemote.transact(TRANSACTION_ADD_MAVLINK_OBSERVER, data, null, 1);
                } catch (Throwable e) {
                    data.recycle();
                    throw e;
                }

                data.recycle();
            }

            @Override
            public IBinder asBinder() {
                return this.mRemote;
            }

            @Override
            public void executeAction(Action action, ICommandListener commandListener) throws RemoteException {
                IBinder binder;
                Parcel data = Parcel.obtain();
                Parcel reply = Parcel.obtain();

                data.writeInterfaceToken(DESCRIPTOR);

                if (action != null) {
                    data.writeInt(1);
                    action.writeToParcel(data, 0);
                } else {
                    data.writeInt(0);
                }

                if (commandListener != null) {
                    binder = commandListener.asBinder();
                } else {
                    binder = null;
                }

                try {
                    data.writeStrongBinder(binder);
                    this.mRemote.transact(TRANSACTION_EXECUTE_ACTION, data, reply, 0);
                    reply.readException();
                    if (reply.readInt() != 0 && action != null) {
                        action.readFromParcel(reply);
                    }
                } catch (RemoteException e) {
                    data.recycle();
                    reply.recycle();
                    throw e;
                }

                data.recycle();
                reply.recycle();
            }

            @Override
            public void executeAsyncAction(Action action, ICommandListener commandListener) throws RemoteException {
                IBinder binder = null;
                Parcel data = Parcel.obtain();
                data.writeInterfaceToken(DESCRIPTOR);

                if (action != null) {
                    data.writeInt(1);
                    action.writeToParcel(data, 0);
                } else {
                    data.writeInt(0);
                }

                if (commandListener != null) {
                    binder = commandListener.asBinder();
                }

                try {
                    data.writeStrongBinder(binder);
                    this.mRemote.transact(TRANSACTION_EXECUTE_ASYNC_ACTION, data, null, 1);
                } catch (RemoteException e) {
                    data.recycle();
                    throw e;
                }

                data.recycle();
            }

            @Override
            public Bundle getAttribute(String attributeName) throws RemoteException {
                Bundle bundle = new Bundle();
                Parcel data = Parcel.obtain();
                Parcel reply = Parcel.obtain();
                data.writeInterfaceToken(DESCRIPTOR);

                try {
                    data.writeInterfaceToken(DESCRIPTOR);
                    data.writeString(attributeName);
                    this.mRemote.transact(TRANSACTION_GET_ATTRIBUTE, data, reply, 0);
                    reply.readException();
                    if (reply.readInt() != 0) {
                        bundle = Bundle.CREATOR.createFromParcel(reply);
                    }
                } catch (RemoteException e) {
                    reply.recycle();
                    data.recycle();
                    throw e;
                }

                reply.recycle();
                data.recycle();
                return bundle;
            }

            @Override
            public void performAction(Action action) throws RemoteException {
                Parcel data = Parcel.obtain();
                Parcel reply = Parcel.obtain();
                data.writeInterfaceToken(DESCRIPTOR);
                if (action != null) {
                    data.writeInt(1);
                    action.writeToParcel(data, 0);
                } else {
                    data.writeInt(0);
                }

                try {
                    this.mRemote.transact(TRANSACTION_PERFORM_ACTION, data, reply, 0);
                    reply.readException();
                    if (reply.readInt() != 0 && action != null) {
                        action.readFromParcel(reply);
                    }
                } catch (RemoteException e) {
                    reply.recycle();
                    data.recycle();
                    throw e;
                }

                reply.recycle();
                data.recycle();
            }

            @Override
            public void performAsyncAction(Action action) throws RemoteException {
                Parcel parcel = Parcel.obtain();
                parcel.writeInterfaceToken(DESCRIPTOR);

                if (action != null) {
                    parcel.writeInt(1);
                    action.writeToParcel(parcel, 0);
                } else {
                    parcel.writeInt(0);
                }

                try {
                    this.mRemote.transact(TRANSACTION_PERFORM_ASYNC_ACTION, parcel, null, 1);
                } catch (RemoteException e) {
                    parcel.recycle();
                    throw e;
                }

                parcel.recycle();
            }

            @Override
            public void removeAttributesObserver(IObserver observer) throws RemoteException {
                IBinder binder = null;
                Parcel data = Parcel.obtain();
                data.writeInterfaceToken(DESCRIPTOR);

                if (observer != null) {
                    binder = observer.asBinder();
                }

                try {
                    data.writeStrongBinder(binder);
                    this.mRemote.transact(TRANSACTION_REMOVE_ATTRIBUTES_OBSERVER, data, null, 1);
                } catch (RemoteException e) {
                    data.recycle();
                    throw e;
                }

                data.recycle();
            }

            @Override
            public void removeMavlinkObserver(IMavlinkObserver mavlinkObserver) throws RemoteException {
                IBinder binder = null;
                Parcel data = Parcel.obtain();
                data.writeInterfaceToken(DESCRIPTOR);

                if (mavlinkObserver != null) {
                    binder = mavlinkObserver.asBinder();
                }

                try {
                    data.writeStrongBinder(binder);
                    this.mRemote.transact(TRANSACTION_REMOVE_MAVLINK_OBSERVER, data, null, 1);
                } catch (RemoteException e) {
                    data.recycle();
                    throw e;
                }

                data.recycle();
            }
        }
    }
}
