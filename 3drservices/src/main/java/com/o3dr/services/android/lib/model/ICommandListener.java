package com.o3dr.services.android.lib.model;


import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

import org.jetbrains.annotations.NotNull;

public interface ICommandListener extends IInterface {
    void onError(int errorCode) throws RemoteException;

    void onSuccess() throws RemoteException;

    void onTimeout() throws RemoteException;

    abstract class Stub extends Binder implements ICommandListener {
        private static final String DESCRIPTOR = "com.o3dr.services.android.lib.model.ICommandListener";
        private static final int TRANSACTION_ON_ERROR = 2;
        private static final int TRANSACTION_ON_SUCCESS = 1;
        private static final int TRANSACTION_ON_TIMEOUT = 3;

        Stub() {
            this.attachInterface(this, DESCRIPTOR);
        }

        @Override
        public IBinder asBinder() {
            return this;
        }

        @Override
        public boolean onTransact(int code, @NotNull Parcel data, Parcel reply, int flags) throws RemoteException {
            boolean transacted = true;
            switch (code) {
                case TRANSACTION_ON_SUCCESS:
                    data.enforceInterface(DESCRIPTOR);
                    this.onSuccess();
                    break;
                case TRANSACTION_ON_ERROR:
                    data.enforceInterface(DESCRIPTOR);
                    this.onError(data.readInt());
                    break;
                case TRANSACTION_ON_TIMEOUT:
                    data.enforceInterface(DESCRIPTOR);
                    this.onTimeout();
                    break;
                case 1598968902:
                    reply.writeString(DESCRIPTOR);
                    break;
                default:
                    transacted = super.onTransact(code, data, reply, flags);
            }
            return transacted;
        }

        public static ICommandListener asInterface(IBinder binder) {
            ICommandListener listener;
            if (binder == null) {
                listener = null;
            } else {
                IInterface localInterface = binder.queryLocalInterface(DESCRIPTOR);
                if (localInterface instanceof ICommandListener) {
                    listener = (ICommandListener) localInterface;
                } else {
                    listener = new Proxy(binder);
                }
            }

            return listener;
        }

        private static class Proxy implements ICommandListener {
            IBinder mRemote;

            public Proxy(IBinder remote) {
                mRemote = remote;
            }

            @Override
            public IBinder asBinder() {
                return this.mRemote;
            }

            @Override
            public void onError(int errorCode) throws RemoteException {
                Parcel parcel = Parcel.obtain();

                try {
                    parcel.writeInterfaceToken(DESCRIPTOR);
                    parcel.writeInt(errorCode);
                    this.mRemote.transact(TRANSACTION_ON_ERROR, parcel, null, 1);
                } finally {
                    parcel.recycle();
                }
            }

            @Override
            public void onSuccess() throws RemoteException {
                Parcel parcel = Parcel.obtain();

                try {
                    parcel.writeInterfaceToken(DESCRIPTOR);
                    this.mRemote.transact(TRANSACTION_ON_SUCCESS, parcel, null, 1);
                } finally {
                    parcel.recycle();
                }
            }

            @Override
            public void onTimeout() throws RemoteException {
                Parcel parcel = Parcel.obtain();

                try {
                    parcel.writeInterfaceToken(DESCRIPTOR);
                    this.mRemote.transact(TRANSACTION_ON_TIMEOUT, parcel, null, 1);
                } finally {
                    parcel.recycle();
                }
            }


        }
    }
}
