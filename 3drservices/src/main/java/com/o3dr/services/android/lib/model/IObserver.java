package com.o3dr.services.android.lib.model;

import android.os.IInterface;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

import org.jetbrains.annotations.NotNull;

public interface IObserver extends IInterface {

    void onAttributeUpdated(String attribute, Bundle bundle) throws RemoteException;

    abstract class Stub extends Binder implements IObserver {
        private static final String DESCRIPTOR = "com.o3dr.services.android.lib.model.IObserver";
        private static final int TRANSACTION_ON_ATTRIBUTE_UPDATED = 1;

        public Stub() {
            this.attachInterface(this, DESCRIPTOR);
        }

        @Override
        public IBinder asBinder() {
            return this;
        }

        @Override
        public boolean onTransact(int code, @NotNull Parcel data, Parcel reply, int flags) throws RemoteException {
            boolean transaction = true;
            switch (code) {
                case TRANSACTION_ON_ATTRIBUTE_UPDATED:
                    data.enforceInterface(DESCRIPTOR);
                    String attributeName = data.readString();
                    Bundle bundle;
                    if (data.readInt() != 0) {
                        bundle = Bundle.CREATOR.createFromParcel(data);
                    } else {
                        bundle = null;
                    }

                    this.onAttributeUpdated(attributeName, bundle);
                break;
                case 1598968902:
                    reply.writeString(DESCRIPTOR);
                    break;
                default:
                    transaction = super.onTransact(code, data, reply, flags);
            }

            return transaction;
        }

        public static IObserver asInterface(IBinder iBinder){
            IObserver observer;
            if (iBinder == null) {
                observer = null;
            } else {
                IInterface localInterface = iBinder.queryLocalInterface(DESCRIPTOR);
                if (localInterface instanceof IObserver) {
                    observer = (IObserver) localInterface;
                } else {
                    observer = new Proxy(iBinder);
                }
            }

            return observer;
        }


        private static class Proxy implements IObserver {
            private IBinder mRemote;

            public Proxy(IBinder remote) {
                mRemote = remote;
            }

            @Override
            public IBinder asBinder() {
                return mRemote;
            }

            @Override
            public void onAttributeUpdated(String attribute, Bundle bundle) throws RemoteException {
                Parcel updater = Parcel.obtain();
                try {
                    updater.writeInterfaceToken(DESCRIPTOR);
                    updater.writeString(attribute);

                    if (bundle != null) {
                        updater.writeInt(1);
                        bundle.writeToParcel(updater, 0);
                    } else {
                        updater.writeInt(0);
                    }

                    this.mRemote.transact(TRANSACTION_ON_ATTRIBUTE_UPDATED, updater, null, 1);
                } catch (RemoteException e) {
                    updater.recycle();
                    throw e;
                }

                updater.recycle();
            }
        }
    }
}
