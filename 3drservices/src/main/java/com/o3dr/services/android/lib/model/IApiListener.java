package com.o3dr.services.android.lib.model;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.o3dr.services.android.lib.drone.connection.ConnectionResult;

import org.jetbrains.annotations.NotNull;

public interface IApiListener extends IInterface {
    void onConnectionFailed(ConnectionResult connectionResult) throws RemoteException;
    int getApiVersionCode() throws RemoteException;
    int getClientVersionCode() throws RemoteException;

    abstract class Stub extends Binder implements IApiListener {
        private static final String DESCRIPTOR = "com.o3dr.services.android.lib.model.IApiListener";
        static final int TRANSACTION_GET_API_VERSION_CODE = 1;
        static final int TRANSACTION_GET_CLIENT_VERSION_CODE = 3;
        static final int TRANSACTION_ON_CONNECTION_FAILED = 2;

        public Stub() {
            this.attachInterface(this, DESCRIPTOR);
        }

        public IBinder asBinder()  {
            return this;
        }

        @Override
        public  boolean onTransact(int code, @NotNull Parcel parcel, Parcel reply, int flags)  throws RemoteException {
            boolean transact = true;
            switch (code) {
                case TRANSACTION_GET_API_VERSION_CODE:
                    parcel.enforceInterface(DESCRIPTOR);
                    code = this.getApiVersionCode();
                    reply.writeNoException();
                    reply.writeInt(code);
                break;
                case TRANSACTION_ON_CONNECTION_FAILED:
                    parcel.enforceInterface(DESCRIPTOR);
                    ConnectionResult connectionResult;
                    if (parcel.readInt() != 0) {
                        connectionResult = ConnectionResult.CREATOR.createFromParcel(parcel);
                    } else {
                        connectionResult = null;
                    }

                    this.onConnectionFailed(connectionResult);
                break;
            case TRANSACTION_GET_CLIENT_VERSION_CODE:
                    parcel.enforceInterface(DESCRIPTOR);
                    code = this.getClientVersionCode();
                    reply.writeNoException();
                    reply.writeInt(code);
                break;
                case 1598968902:
                    reply.writeString(DESCRIPTOR);
                    break;
                default:
                    transact = super.onTransact(code, parcel, reply, flags);
            }

            return transact;
        }

        public static IApiListener asInterface(IBinder binder) {
            IApiListener listener;
            if (binder == null) {
                listener = null;
            } else {
                IInterface iInterface = binder.queryLocalInterface(DESCRIPTOR);
                if (iInterface instanceof IApiListener) {
                    listener = (IApiListener)iInterface;
                } else {
                    listener = new Proxy(binder);
                }
            }

            return listener;
        }

        private static class Proxy implements IApiListener {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                mRemote = remote;
            }

            @Override
            public int getApiVersionCode() throws RemoteException {
                Parcel parcel = Parcel.obtain();
                Parcel reply = Parcel.obtain();

                int versionCode;
                try {
                    parcel.writeInterfaceToken(DESCRIPTOR);
                    this.mRemote.transact(TRANSACTION_GET_API_VERSION_CODE, parcel, reply, 0);
                    reply.readException();
                    versionCode = reply.readInt();
                } finally {
                    reply.recycle();
                    parcel.recycle();
                }

                return versionCode;
            }

            @Override
            public int getClientVersionCode() throws RemoteException {
                Parcel parcel = Parcel.obtain();
                Parcel reply = Parcel.obtain();

                int version;
                try {
                    parcel.writeInterfaceToken(DESCRIPTOR);
                    this.mRemote.transact(TRANSACTION_GET_CLIENT_VERSION_CODE, parcel, reply, 0);
                    reply.readException();
                    version = reply.readInt();
                } finally {
                    reply.recycle();
                    parcel.recycle();
                }

                return version;
            }

            @Override
            public IBinder asBinder()  {
                return this.mRemote;
            }

            @Override
            public void onConnectionFailed(ConnectionResult connectionResult) throws RemoteException {
                Parcel parcel = Parcel.obtain();
                try {
                    parcel.writeInterfaceToken(DESCRIPTOR);

                    if (connectionResult != null) {
                        parcel.writeInt(1);
                        connectionResult.writeToParcel(parcel, 0);
                    } else {
                        parcel.writeInt(0);
                    }

                    this.mRemote.transact(TRANSACTION_ON_CONNECTION_FAILED, parcel, null, 1);
                } catch (RemoteException e) {
                    parcel.recycle();
                    throw e;
                }

                parcel.recycle();
            }
        }
    }
}