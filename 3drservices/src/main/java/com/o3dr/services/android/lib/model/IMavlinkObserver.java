package com.o3dr.services.android.lib.model;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

import com.o3dr.services.android.lib.mavlink.MavlinkMessageWrapper;

import org.jetbrains.annotations.NotNull;

public interface IMavlinkObserver extends IInterface {

    void onMavlinkMessageReceived(MavlinkMessageWrapper wrapper) throws RemoteException;

    abstract class Stub extends Binder implements IMavlinkObserver {
        private static final String DESCRIPTOR = "com.o3dr.services.android.lib.model.IMavlinkObserver";
        private static final int TRANSACTION_ON_MAVLINK_MESSAGE_RECEIVED = 1;

        public Stub() {
            this.attachInterface(this, DESCRIPTOR);
        }

        @Override
        public IBinder asBinder()  {
            return this;
        }

        @Override
        public boolean onTransact(int code, @NotNull Parcel data, Parcel reply, int flags)  throws RemoteException{
            boolean result = true;
            switch (code) {
                case TRANSACTION_ON_MAVLINK_MESSAGE_RECEIVED:
                    data.enforceInterface(DESCRIPTOR);
                    MavlinkMessageWrapper wrapper;
                    if (data.readInt() != 0) {
                        wrapper = MavlinkMessageWrapper.CREATOR.createFromParcel(data);
                    } else {
                        wrapper = null;
                    }

                    this.onMavlinkMessageReceived(wrapper);
                break;
                case 1598968902 :
                    reply.writeString(DESCRIPTOR);
                    break;
                default:
                    result = super.onTransact(code, data, reply, flags);
            }

            return result;
        }

        public static IMavlinkObserver asInterface(IBinder binder) {
            IMavlinkObserver observer;
            if (binder == null) {
                observer = null;
            } else {
                IInterface localInterface = binder.queryLocalInterface(DESCRIPTOR);
                if (localInterface instanceof IMavlinkObserver) {
                    observer = (IMavlinkObserver)localInterface;
                } else {
                    observer = new Proxy(binder);
                }
            }

            return observer;
        }

        private static class Proxy implements IMavlinkObserver {
            IBinder mRemote;

            public Proxy(IBinder remote) {
                mRemote = remote;
            }

            @Override
            public IBinder asBinder()  {
                return this.mRemote;
            }

            @Override
            public void onMavlinkMessageReceived(MavlinkMessageWrapper messageWrapper) throws RemoteException{
                Parcel data = Parcel.obtain();
                data.writeInterfaceToken(DESCRIPTOR);
                if (messageWrapper != null) {
                    data.writeInt(1);
                    messageWrapper.writeToParcel(data, 0);
                } else {
                    data.writeInt(0);
                }

                try {
                    this.mRemote.transact(TRANSACTION_ON_MAVLINK_MESSAGE_RECEIVED, data, null, 1);
                } catch (RemoteException e) {
                    data.recycle();
                    throw e;
                }

                data.recycle();
            }
        }
    }
}
