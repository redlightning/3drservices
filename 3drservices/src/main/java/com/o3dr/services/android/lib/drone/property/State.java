package com.o3dr.services.android.lib.drone.property;

import android.os.Parcel;
import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

import timber.log.Timber;

/**
 * Created by fhuya on 10/28/14.
 */
public class State implements DroneAttribute {
    public static final int INVALID_MAVLINK_VERSION = -1;
    private boolean armed;
    private String calibrationStatus;
    private EkfStatus ekfStatus;
    private long flightStartTime;
    private boolean isConnected;
    private boolean isFlying;
    private boolean telemetryLive;
    private int mavlinkVersion = INVALID_MAVLINK_VERSION;
    private VehicleMode vehicleMode = VehicleMode.UNKNOWN;
    private JSONObject vehicleUid;
    private String failsafeWarning;
    private Vibration vehicleVibration;
    private boolean calibrating;

    public State(){}

    public State(boolean isConnected, VehicleMode mode, boolean armed, boolean flying, String failsafeWarning,
                 int mavlinkVersion, String calibrationStatus, long flightStartTime){
        this.isConnected = isConnected;
        this.vehicleMode = mode;
        this.armed = armed;
        this.isFlying = flying;
        this.flightStartTime = flightStartTime;
        this.failsafeWarning = failsafeWarning;
        this.mavlinkVersion = mavlinkVersion;
        this.calibrationStatus = calibrationStatus;
        this.setEkfStatus(new EkfStatus());
        this.vehicleVibration = new Vibration();
        this.vehicleUid = new JSONObject();
    }

    public State(
            boolean isConnected,
            VehicleMode vehicleMode ,
            boolean armed,
            boolean flying,
            String autopilotErrorId,
            int mavLinkVersion,
            String calibrationStatus,
            long flightStartTime,
            EkfStatus ekfStatus ,
            boolean isTelemetryLive,
            Vibration vibration
    ) {
        this.mavlinkVersion = INVALID_MAVLINK_VERSION;
        this.vehicleMode = VehicleMode.UNKNOWN;
        this.setEkfStatus(new EkfStatus());
        this.vehicleVibration = new Vibration();
        this.vehicleUid = new JSONObject();
        this.isConnected = isConnected;
        this.armed = armed;
        this.isFlying = flying;
        this.flightStartTime = flightStartTime;
        this.failsafeWarning = autopilotErrorId;
        this.mavlinkVersion = mavLinkVersion;
        this.calibrationStatus = calibrationStatus;
        this.setEkfStatus(ekfStatus);
        this.vehicleMode = vehicleMode;
        this.telemetryLive = isTelemetryLive;
        this.vehicleVibration = vibration;
    }

    public EkfStatus getEkfStatus() {
        return ekfStatus;
    }

    public void setEkfStatus(EkfStatus ekfStatus) {
        this.ekfStatus = ekfStatus;
    }

    public boolean isTelemetryLive() {
        return telemetryLive;
    }

    public void setTelemetryLive(boolean telemetryLive) {
        this.telemetryLive = telemetryLive;
    }

    public JSONObject getVehicleUid() {
        return vehicleUid;
    }

    public void setVehicleUid(JSONObject vehicleUid) {
        this.vehicleUid = vehicleUid;
    }

    public Vibration getVehicleVibration() {
        return vehicleVibration;
    }

    public void setVehicleVibration(Vibration vehicleVibration) {
        this.vehicleVibration = vehicleVibration;
    }

    public void setCalibrating(boolean calibrating) {
        this.calibrating = calibrating;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void setConnected(boolean isConnected) {
        this.isConnected = isConnected;
    }

    public void setArmed(boolean armed) {
        this.armed = armed;
    }

    public void setFlying(boolean isFlying) {
        this.isFlying = isFlying;
    }

    public void setCalibrationStatus(String calibrationStatus) {
        this.calibrationStatus = calibrationStatus;
    }

    public void setVehicleMode(VehicleMode vehicleMode) {
        this.vehicleMode = vehicleMode;
    }

    public void setMavlinkVersion(int mavlinkVersion) {
        this.mavlinkVersion = mavlinkVersion;
    }

    public boolean isArmed() {
        return armed;
    }

    public boolean isFlying() {
        return isFlying;
    }

    public VehicleMode getVehicleMode() {
        return vehicleMode;
    }

    public String getFailsafeWarning() {
        return failsafeWarning;
    }

    public void setFailsafeWarning(String failsafeWarning) {
        this.failsafeWarning = failsafeWarning;
    }

    public boolean isWarning(){
        return TextUtils.isEmpty(failsafeWarning);
    }

    public boolean isCalibrating(){
        return calibrationStatus != null;
    }

    public void setCalibration(String message){
        this.calibrationStatus = message;
    }

    public String getCalibrationStatus(){
        return this.calibrationStatus;
    }

    public int getMavlinkVersion() {
        return mavlinkVersion;
    }

    public long getFlightStartTime() {
        return flightStartTime;
    }

    public void setFlightStartTime(long flightStartTime) {
        this.flightStartTime = flightStartTime;
    }

    public void addToVehicleUid(String uidLabel, String uid) {
        try {
            this.vehicleUid.put(uidLabel, uid);
        } catch (JSONException e) {
            Timber.e(e);
        }
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(isConnected ? (byte) 1: (byte) 0);
        dest.writeByte(armed ? (byte) 1 : (byte) 0);
        dest.writeByte(isFlying ? (byte) 1 : (byte) 0);
        dest.writeString(this.calibrationStatus);
        dest.writeParcelable(this.vehicleMode, 0);
        dest.writeString(this.failsafeWarning);
        dest.writeInt(this.mavlinkVersion);
        dest.writeLong(this.flightStartTime);
        dest.writeParcelable(this.getEkfStatus(), 0);
        dest.writeByte(this.telemetryLive? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.vehicleVibration, 0);
        dest.writeString(this.vehicleUid.toString());
    }

    private State(Parcel in) {
        this.isConnected = in.readByte() != 0;
        this.armed = in.readByte() != 0;
        this.isFlying = in.readByte() != 0;
        this.calibrationStatus = in.readString();
        this.vehicleMode = in.readParcelable(VehicleMode.class.getClassLoader());
        this.failsafeWarning = in.readString();
        this.mavlinkVersion = in.readInt();
        this.flightStartTime = in.readLong();
        this.setEkfStatus(in.readParcelable(EkfStatus.class.getClassLoader()));
        this.telemetryLive = (int)in.readByte() != 0;
        this.vehicleVibration = in.readParcelable(Vibration.class.getClassLoader());

        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(in.readString());
        } catch ( JSONException e)  {
            jsonObject = new JSONObject();
        }

        this.vehicleUid = jsonObject;

    }

    public static final Creator<State> CREATOR = new Creator<State>() {
        public State createFromParcel(Parcel source) {
            return new State(source);
        }

        public State[] newArray(int size) {
            return new State[size];
        }
    };
}
