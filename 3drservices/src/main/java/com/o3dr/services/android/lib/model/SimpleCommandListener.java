package com.o3dr.services.android.lib.model;

import timber.log.Timber;

/**
 * Basic command listener implementation.
 * Overrides the methods as needed to receive the command execution status notification.
 * Created by Fredia Huya-Kouadio on 6/24/15.
 */
public class SimpleCommandListener extends AbstractCommandListener {
    @Override
    public void onSuccess() {
        Timber.v("SimpleCommandListener success");
    }

    @Override
    public void onError(int executionError) {
        Timber.v("SimpleCommandListener error: %s", executionError);
    }

    @Override
    public void onTimeout() {
        Timber.v("SimpleCommandListener timeout");
    }
}
