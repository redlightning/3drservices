package com.o3dr.services.android.lib.model;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

import androidx.annotation.NonNull;

public interface IDroidPlannerServices extends IInterface {
    Bundle[] getConnectedApps(String appString) throws RemoteException;
    IDroneApi registerDroneApi(IApiListener apiListener, String name) throws RemoteException;
    void releaseDroneApi(IDroneApi droneApi) throws RemoteException;


    abstract class Stub extends Binder implements IDroidPlannerServices {
        private static final String DESCRIPTOR = "com.o3dr.services.android.lib.model.IDroidPlannerServices";
        private static final int TRANSACTION_GET_API_VERSION_CODE = 3;
        private static final int TRANSACTION_GET_CONNECTED_APPS = 5;
        private static final int TRANSACTION_GET_SERVICE_VERSION_CODE = 1;
        private static final int TRANSACTION_REGISTER_DRONE_API = 4;
        private static final int TRANSACTION_RELEASE_DRONE_API = 2;

        public Stub() {
            this.attachInterface(this, DESCRIPTOR);
        }

        @Override
        public IBinder asBinder() {
            return this;
        }

        public static IDroidPlannerServices asInterface(IBinder binder) {
            IDroidPlannerServices plannerServices;
            if (binder == null) {
                plannerServices = null;
            } else {
                IInterface localInterface = binder.queryLocalInterface(DESCRIPTOR);
                if (localInterface instanceof IDroidPlannerServices) {
                    plannerServices = (IDroidPlannerServices)localInterface;
                } else {
                    plannerServices = new Proxy(binder);
                }
            }

            return plannerServices;
        }

        @Override
        public boolean onTransact(int code, @NonNull Parcel data, Parcel reply, int flags) throws RemoteException {
            boolean successful;
            switch (code) {
                case TRANSACTION_GET_SERVICE_VERSION_CODE:
                    data.enforceInterface(DESCRIPTOR);
                    code = new Proxy(this).getServiceVersionCode();
                    reply.writeNoException();
                    reply.writeInt(code);
                    successful = true;
                    break;
                    case TRANSACTION_RELEASE_DRONE_API:
                    data.enforceInterface(DESCRIPTOR);
                    this.releaseDroneApi(IDroneApi.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    successful = true;
                    break;
                case TRANSACTION_GET_API_VERSION_CODE:
                    data.enforceInterface(DESCRIPTOR);
                    code = new Proxy(this).getApiVersionCode();
                    reply.writeNoException();
                    reply.writeInt(code);
                    successful = true;
                    break;
                case TRANSACTION_REGISTER_DRONE_API:
                    data.enforceInterface(DESCRIPTOR);
                    IDroneApi droneApi = this.registerDroneApi(IApiListener.Stub.asInterface(data.readStrongBinder()), data.readString());
                    reply.writeNoException();
                    IBinder binder;
                    if (droneApi != null) {
                        binder = droneApi.asBinder();
                    } else {
                        binder = null;
                    }

                    reply.writeStrongBinder(binder);
                    successful = true;
                break;
            case TRANSACTION_GET_CONNECTED_APPS:
                    data.enforceInterface(DESCRIPTOR);
                    Bundle[] connectedApps = this.getConnectedApps(data.readString());
                    reply.writeNoException();
                    reply.writeTypedArray(connectedApps, 1);
                    successful = true;
                break;
                case 1598968902:
                    reply.writeString(DESCRIPTOR);
                    successful = true;
                    break;
                default:
                    successful = super.onTransact(code, data, reply, flags);
            }

            return successful;
        }

        private static class Proxy implements IDroidPlannerServices {
            IBinder mRemote;

            public Proxy(IBinder remote) {
                mRemote = remote;
            }

            int getApiVersionCode() throws RemoteException {
                Parcel data = Parcel.obtain();
                Parcel reply = Parcel.obtain();

                int versionCode;
                try {
                    data.writeInterfaceToken(DESCRIPTOR);
                    mRemote.transact(TRANSACTION_GET_API_VERSION_CODE, data, reply, 0);
                    reply.readException();
                    versionCode = reply.readInt();
                } finally {
                    reply.recycle();
                    data.recycle();
                }

                return versionCode;
            }

            int getServiceVersionCode() throws RemoteException {
                Parcel data = Parcel.obtain();
                Parcel reply = Parcel.obtain();

                int serviceVersionCode;
                try {
                    data.writeInterfaceToken(DESCRIPTOR);
                    mRemote.transact(TRANSACTION_GET_SERVICE_VERSION_CODE, data, reply, 0);
                    reply.readException();
                    serviceVersionCode = reply.readInt();
                } finally {
                    reply.recycle();
                    data.recycle();
                }

                return serviceVersionCode;
            }

            public IBinder asBinder()  {
                return mRemote;
            }


            @Override
            public Bundle[] getConnectedApps(String appString) throws RemoteException {
                Parcel data = Parcel.obtain();
                Parcel reply = Parcel.obtain();

                Bundle[]  bundles;
                try {
                    data.writeInterfaceToken(DESCRIPTOR);
                    data.writeString(appString);
                    mRemote.transact(TRANSACTION_GET_CONNECTED_APPS, data, reply, 0);
                    reply.readException();
                    bundles = reply.createTypedArray(Bundle.CREATOR);
                } finally {
                    reply.recycle();
                    data.recycle();
                }

                return bundles;
            }

            @Override
            public IDroneApi registerDroneApi(IApiListener apiListener, String name) throws RemoteException {
                Parcel data = Parcel.obtain();
                Parcel reply = Parcel.obtain();
                IDroneApi droneApi;
                    data.writeInterfaceToken(DESCRIPTOR);
                IBinder binder;
            if (apiListener != null) {
                binder = apiListener.asBinder();
            } else {
                binder = null;
            }

            try {
                data.writeStrongBinder(binder);
                data.writeString(name);
                mRemote.transact(TRANSACTION_REGISTER_DRONE_API, data, reply, 0);
                reply.readException();
                droneApi = IDroneApi.Stub.asInterface(reply.readStrongBinder());
            } catch (RemoteException e) {
                reply.recycle();
                data.recycle();
                throw e;
            }

            reply.recycle();
            data.recycle();
            return droneApi;
            }

            @Override
            public void releaseDroneApi(IDroneApi droneApi) throws RemoteException {
                Parcel data = Parcel.obtain();
                Parcel reply = Parcel.obtain();

                data.writeInterfaceToken(DESCRIPTOR);
                IBinder binder = null;
                if (droneApi != null) {
                    binder = droneApi.asBinder();
                }

                try {
                    data.writeStrongBinder(binder);
                    mRemote.transact(TRANSACTION_RELEASE_DRONE_API, data, reply, 0);
                    reply.readException();
                } catch (RemoteException e) {
                    reply.recycle();
                    data.recycle();
                    throw e;
                }

                reply.recycle();
                data.recycle();
            }
        }
    }
}