package org.droidplanner.services.android.impl.core.survey;

import org.jetbrains.annotations.NotNull;

public class CameraInfo {
	private String name = "Canon SX260";
	private Double sensorWidth = 6.12;
	private Double sensorHeight = 4.22;
	private Double sensorResolution = 12.1;
	private Double focalLength = 5.0;
	private Double overlap = 50.0;
	private Double sidelap = 60.0;
	private boolean isInLandscapeOrientation = true;

	public Double getSensorLateralSize() {
		if (isInLandscapeOrientation()) {
			return getSensorWidth();
		} else {
			return getSensorHeight();
		}
	}

	public Double getSensorLongitudinalSize() {
		if (isInLandscapeOrientation()) {
			return getSensorHeight();
		} else {
			return getSensorWidth();
		}
	}

	@NotNull
    @Override
	public String toString() {
		return "Camera:"+ getName() +" ImageWidth:" + getSensorWidth() + " ImageHeight:" + getSensorHeight() + " FocalLength:"
				+ getFocalLength() + " Overlap:" + getOverlap() + " Sidelap:" + getSidelap()
				+ " isInLandscapeOrientation:" + isInLandscapeOrientation();

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getSensorWidth() {
		return sensorWidth;
	}

	public void setSensorWidth(Double sensorWidth) {
		this.sensorWidth = sensorWidth;
	}

	public Double getSensorHeight() {
		return sensorHeight;
	}

	public void setSensorHeight(Double sensorHeight) {
		this.sensorHeight = sensorHeight;
	}

	public Double getSensorResolution() {
		return sensorResolution;
	}

	public void setSensorResolution(Double sensorResolution) {
		this.sensorResolution = sensorResolution;
	}

	public Double getFocalLength() {
		return focalLength;
	}

	public void setFocalLength(Double focalLength) {
		this.focalLength = focalLength;
	}

	public Double getOverlap() {
		return overlap;
	}

	public void setOverlap(Double overlap) {
		this.overlap = overlap;
	}

	public Double getSidelap() {
		return sidelap;
	}

	public void setSidelap(Double sidelap) {
		this.sidelap = sidelap;
	}

	public boolean isInLandscapeOrientation() {
		return isInLandscapeOrientation;
	}

	public void setInLandscapeOrientation(boolean inLandscapeOrientation) {
		isInLandscapeOrientation = inLandscapeOrientation;
	}
}