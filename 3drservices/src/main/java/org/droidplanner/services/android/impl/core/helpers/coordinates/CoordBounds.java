package org.droidplanner.services.android.impl.core.helpers.coordinates;

import org.droidplanner.services.android.impl.core.helpers.geoTools.GeoTools;
import com.o3dr.services.android.lib.coordinate.LatLong;

import java.util.List;

/**
 * Calculate a rectangle that bounds all inserted points
 */
public class CoordBounds {
	private LatLong sw_3quadrant;
	private LatLong ne_1quadrant;

	public CoordBounds(LatLong point) {
		include(point);
	}

	public CoordBounds(List<LatLong> points) {
		for (LatLong point : points) {
			include(point);
		}
	}

	public void include(LatLong point) {
		if ((getSw_3quadrant() == null) || (getNe_1quadrant() == null)) {
			setNe_1quadrant(new LatLong(point));
			setSw_3quadrant(new LatLong(point));
		} else {
			if (point.getLongitude() > getNe_1quadrant().getLongitude()) {
				getNe_1quadrant().setLongitude(point.getLongitude());
			}
			if (point.getLatitude() > getNe_1quadrant().getLatitude()) {
				getNe_1quadrant().setLatitude(point.getLatitude());
			}
			if (point.getLongitude() < getSw_3quadrant().getLongitude()) {
				getSw_3quadrant().setLongitude(point.getLongitude());
			}
			if (point.getLatitude() < getSw_3quadrant().getLatitude()) {
				getSw_3quadrant().setLatitude(point.getLatitude());
			}
		}
	}

	public double getDiag() {
		return GeoTools.latToMeters(GeoTools.getAproximatedDistance(getNe_1quadrant(), getSw_3quadrant()));
	}

	public LatLong getMiddle() {
		return (new LatLong((getNe_1quadrant().getLatitude() + getSw_3quadrant().getLatitude()) / 2,
				(getNe_1quadrant().getLongitude() + getSw_3quadrant().getLongitude()) / 2));

	}

	public LatLong getSw_3quadrant() {
		return sw_3quadrant;
	}

	public void setSw_3quadrant(LatLong sw_3quadrant) {
		this.sw_3quadrant = sw_3quadrant;
	}

	public LatLong getNe_1quadrant() {
		return ne_1quadrant;
	}

	public void setNe_1quadrant(LatLong ne_1quadrant) {
		this.ne_1quadrant = ne_1quadrant;
	}
}
